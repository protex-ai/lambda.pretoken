const axios = require("axios");
const gql = require("graphql-tag");
const graphql = require("graphql");
const { print } = graphql;

const queryTemplate = gql`
    query GetRole($email: String!) {
         users(where: {email: {_eq: $email}}) {
            role
            site_id
            id
            first_name
            last_name
            profile_pic_url
    }
}
`;

exports.handler = async function(event, context)  {
    const parameterEmail = event.request.userAttributes.email;
    // const parameterEmail = event.userAttributes.email;
    let resultRole ='';
    let resultSiteId ='';
    let resultUserId='';
    let resultFirstName='';
    let resultLastName='';
    let resultProfilePicUrl='';
    try {
        const graphqlData = await axios({
            url: "http://ec2-52-215-43-1.eu-west-1.compute.amazonaws.com/v1/graphql",
            method: 'post',
            headers: {
                "content-type": "application/json",
                "x-hasura-admin-secret": "consolelogbetterthanprint",
            },
            data: {
                query: print(queryTemplate),
                variables:{ email: parameterEmail },
            },
        })
     resultRole = graphqlData.data.data.users[0].role
     resultSiteId = graphqlData.data.data.users[0].site_id
     resultUserId = graphqlData.data.data.users[0].id
     resultFirstName= graphqlData.data.data.users[0].first_name;
     resultLastName= graphqlData.data.data.users[0].last_name;
     resultProfilePicUrl= graphqlData.data.data.users[0].profile_pic_url;

    console.log(JSON.stringify(`resultRole -> ${resultRole}, resultSiteId ->${resultSiteId} `));
    }catch(err){
        console.log(err)
    }
        

    event.response = {
        "claimsOverrideDetails": {
            "claimsToAddOrOverride": {
                "https://hasura.io/jwt/claims": JSON.stringify({
                    "x-hasura-user-id": event.request.userAttributes.sub,
                    "x-hasura-default-role": resultRole,
                    "x-hasura-site-id": resultSiteId.toString(),
                    "x-hasura-allowed-roles": ["Staff","Admin","SuperUser","admin","anonymous","SiteAdmin"],
                    "x-hasura-user-id": resultUserId.toString(),
                    "x-hasura-first-name":resultFirstName,
                    "x-hasura-last-name":resultLastName,
                    "x-hasura-profile-url":resultProfilePicUrl,
                })
            }
        }
    }


   return event
}

